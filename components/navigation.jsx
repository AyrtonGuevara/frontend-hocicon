"use client";
import React from 'react';
import {Link as RouterLink} from 'next/link';
import IconButton from '@mui/material/IconButton';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const pages = [
	{name:'Principal',url:'/'},
	{name:'Nueva Noticia',url:'/news/create'}];


function Navigation(){
	const [anchorElNav, setAnchorElNav] = React.useState(null);

 	const handleOpenNavMenu = (event) => {
    	setAnchorElNav(event.currentTarget);
  	};

  	const handleCloseNavMenu = () => {
    	setAnchorElNav(null);
  	};

	return(
			/*<ul>
				<li>
					
					<Link href="/">Principal</Link>
				</li>
				<li>
					<Link href="/news/create">Nueva Noticia</Link>
				</li>
			</ul>*/
			<Box sx={{ flexGrow: 1 ,position: 'sticky', top: 0, zIndex: 1000}}>
		      <AppBar position="static">
		        <Toolbar variant="dense">
		          <Typography variant="h6" color="inherit" component="div">
		            
		          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
		          	<Typography
			            variant="h6"
			            noWrap
			            component="a"
			            sx={{
			              mr: 2,
			              display: { xs: 'none', md: 'flex' },
			              fontFamily: 'monospace',
			              fontWeight: 700,
			              letterSpacing: '.2rem',
			              color: 'inherit',
			            }}
			          >
			            Noticias El Hocicón
			          </Typography>
		            {pages.map((page) => (
		              <Button
		                key={page}
		                onClick={handleCloseNavMenu}
		                sx={{ my: 2, color: 'white', display: 'block' }}
		                component={RouterLink}
                  		href={page.url}
		              >
		                {page.name}
		              </Button>
		            ))}
		          </Box>

		          </Typography>
		        </Toolbar>
		      </AppBar>
		    </Box>
	)
}

export default Navigation;
