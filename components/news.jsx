"use client";
import Container from '@mui/material/Container';
import {useRouter} from "next/navigation";
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

function News({ news }){
	const router = useRouter()

	return(
		<Container sx={{ py: 8 }} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {news.map((carditem) => (
              <Grid item key={carditem.id} xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardMedia
                    component="div"
                    sx={{
                      // 16:9
                      pt: '56.25%',
                    }}
                    image={`http://localhost:3001/files/image/${carditem.imagen_principal}`}
                    alt="Not Image Avaible"
                  />
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography 
                    	gutterBottom variant="h5" 
                    	component="h2"
                    	sx={{
					        overflow: 'hidden',
					        display: '-webkit-box',
					        WebkitBoxOrient: 'vertical',
					        WebkitLineClamp: 2,
					      }}
                    >
                      {carditem.titulo_noticia}
                    </Typography>
                    <Typography
				      sx={{
				        overflow: 'hidden',
				        display: '-webkit-box',
				        WebkitBoxOrient: 'vertical',
				        WebkitLineClamp: 1,
				      }}
				    >
				      {carditem.contenido}
				    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button 
                    	size="small" 
                    	href={`/news/${carditem.id}`}>Ver</Button>
                    <Button 
                    	size="small"
                    	href={`/news/edit/${carditem.id}`}>Modificar</Button>
                    <Button 
                    	size="small" 
                    	onClick={async()=>{
				              const res = await fetch(`http://localhost:3001/news/${carditem.id}`,{
				                method:"DELETE",
				              })
				            router.push("/")
				          }}>Eliminar</Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
	)
}

export default News;