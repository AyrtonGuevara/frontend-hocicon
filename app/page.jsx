"use client";
import News from "@/components/news";
import Button from '@mui/material/Button';
import React, { useEffect, useState,useRef } from 'react';


async function getNews(){
	try{
		const res = await fetch("http://localhost:3001/news",{
			next:{
				revalidate: 3600
			}
		})
		const data = await res.json();
		return data
	}catch(error){
		console.error('Error en la solicitud:', error);
  		throw error; 
	}

}

async function HomePage(){
	const selectRef = useRef(null);
	const inputRef = useRef(null);
	
	const [filter,setFiltrar]=useState("");
	const [order,setOrdenar]=useState("");
	const [news,setNews] = useState([]);

	useEffect(()=>{
		async function fetchData(){
			const new_List = await getNews();
			setNews(new_List);
		}
		fetchData();
	}, []);

	const FilterNews = async (Values)=>{
		Values.preventDefault();
		const order = selectRef.current.value;
  		const filter = inputRef.current.value;

		if(order!==""|| filter!==""){
			let url=(`http://localhost:3001/news?`);
				if(order!==""){
					url+=`limit=${order}`;
					if(filter!==""&&order!==""){
						url+=`&offset=${filter}`;
					}
				}else{
					url+=`offset=${filter}`;
				}
			const resp=await fetch(url,{
				method:"GET",
			});
			const New_new=await resp.json();
			setNews(New_new);
		}
	}
	return (
		<div>
			<h1>Lista de noticias</h1>
			<div>
				<label> buscar por</label>
				<select name="select" ref={selectRef}>
					<option></option>
					<option value="Titulo_noticia">Titulo</option>
					<option value="fecha">Fecha</option>
					<option value="nombre_lugar">Lugar</option>
					<option value="nombre_autor">Autor</option>
				</select>
				<input 
					type="text" 
					name="filtro"
					ref={inputRef}
					/>
				<input type="submit" value="buscar" onClick={FilterNews}/>
			</div>
			<News news={news} />
		</div>
	);
}

export default HomePage