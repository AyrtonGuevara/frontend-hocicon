"use client";
import {useRouter} from "next/navigation";
import React, { useEffect, useState } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

async function getNewsId(id){
	try{
		const res=await fetch(`http://localhost:3001/news/${id}`)
		const data=await res.json()
		return data
	}catch(error){
		console.error('Error:',error);
	}
}

async function Fecha(timestamp) {
  const date = new Date(timestamp);
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Los meses comienzan desde 0
  const year = date.getFullYear();
  return `${day}/${month}/${year}`;
}

async function NewPage({params}){
	const router = useRouter();
	const new_n = await getNewsId(params.id)
	return(
		<ThemeProvider theme={createTheme()}>
	      <CssBaseline />
	      <Container maxWidth="lg">
		    <Paper
		      sx={{
		        position: 'relative',
		        top: '20px',
		        backgroundColor: 'grey.800',
		        color: '#fff',
		        mb: 4,
		        backgroundSize: 'cover',
		        backgroundRepeat: 'no-repeat',
		        backgroundPosition: 'center',
		        backgroundImage: `url(http://localhost:3001/files/image/${new_n[0].imagen_principal})`,
		      }}
		    >
		      {/* Increase the priority of the hero background image */}
		      {<img style={{ display: 'none' }} 
		      src={`http://localhost:3001/files/image/${new_n[0].imagen_principal}`}
		      alt="Image not Avaible"/>}
		      <Box
		        sx={{
		          position: 'absolute',
		          top: 0,
		          bottom: 0,
		          right: 0,
		          left: 0,
		          backgroundColor: 'rgba(0,0,0,.3)',
		        }}
		      />
		      <Grid container>
		        <Grid item md={6}>
		          <Box
		            sx={{
		              position: 'relative',
		              p: { xs: 3, md: 6 },
		              pr: { md: 0 },
		            }}
		          >
		            <Typography component="h1" variant="h3" color="inherit" gutterBottom>
		              {new_n[0].titulo_noticia}
		            </Typography>
		          </Box>
		        </Grid>
		      </Grid>
		    </Paper>
	        <Grid
		    item
		    xs={12}
		    md={8}
		    sx={{
		    	'& .markdown': {
		    		py: 3,
		    	},
		    }}
		    >
		      <Typography variant="h6" gutterBottom>
		        {new_n[0].titulo_noticia}
		      </Typography>
		      <p>
		      	por: {new_n[0].autor.nombre_autor}
		      	<br />
		      	en fecha: {Fecha(new_n[0].fecha)}
		      	<br />
		      	{new_n[0].lugar.nombre_lugar}
		      </p>
		      <Typography component="div" paragraph>
		      	{new_n[0].contenido}
		      	</Typography>
		    </Grid>
	      </Container>
	    </ThemeProvider>
	)
}

export default NewPage;