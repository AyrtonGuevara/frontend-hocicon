"use client"
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { styled } from '@mui/material/styles';


const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});

function CreateNew({params}){
	const router = useRouter();
	const [titulo_noticia, setTitulo_noticia]=useState("");
	const [fecha, setFecha]=useState("");
	const [contenido, setContenido]=useState("");
	const [lugar, setNombre_lugar]=useState("");
	const [autor, setNombre_autor]=useState("");
	const [imagen, setImagen]=useState(null);
	const [nombre_imagen,SetNombre_imagen]=useState("");
	const [showUploadButton, setShowUploadButton] = useState(true);

	useEffect(()=>{
		if(params.id){
			fetch(`http://localhost:3001/news/${params.id}`)
			.then(res=>res.json())
			.then(data=>{
				setTitulo_noticia(data[0].titulo_noticia);
				setContenido(data[0].contenido);
				setFecha(data[0].fecha);
				setNombre_autor(data[0].autor.nombre_autor);
				setNombre_lugar(data[0].lugar.nombre_lugar);
			}).then(setShowUploadButton(false))
			.catch(error=>console.log('Error',error));
		}
	},[]);

	const ImageNew = (event) => {
    	const Imagen = event.target.files[0];
    	setImagen(Imagen);
  	};

  		const formData = new FormData();
    	formData.append('file', imagen);

	const FormCreateNew=async (ValueForm)=>{
		ValueForm.preventDefault();
		if(params.id){
			const resp=await fetch (`http://localhost:3001/news/${params.id}`,{
				method: "PATCH",
				headers:{
				'Content-Type': 'application/json'
			},
			mode:'cors',
			body:JSON.stringify({
				titulo_noticia:titulo_noticia,
				autor:autor,
				contenido:contenido,
				lugar:lugar
			})
			})
			.catch(error => console.error('Error:', error));
		}else{
			const resp_im=await fetch (`http://localhost:3001/files/image`,{
				method: 'POST',
				haders:{
					'Content-Type': 'multipart/json'
				},
				mode:'cors',
				body:formData,
			}).then(response=>{
				response.text().then((filename)=>{
					SetNombre_imagen(filename);
				})
			}).catch(error=>console.error('Error',error));
			

			const resp=await fetch ('http://localhost:3001/news',{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			mode:'cors',
			body:JSON.stringify({
				titulo_noticia:titulo_noticia,
				autor:autor,
				contenido:contenido,
				lugar:lugar,
				imagen_principal:nombre_imagen
			}),
			
			})
			.catch(error => console.error('Error:', error));
		}
		router.push('/');
	}
	return(
		<ThemeProvider theme={createTheme()}>
	      <Container component="main" maxWidth="md">
	        <CssBaseline />
	        <Box
	          sx={{
	            marginTop: 8,
	            display: 'flex',
	            flexDirection: 'column',
	            alignItems: 'center',
	          }}
	        >
	          <Typography component="h1" variant="h5">
	            Crear / Modificar Noticia
	          </Typography>
	          <Box component="form" noValidate onSubmit={FormCreateNew} sx={{ mt: 3 }}>
	            <Grid container spacing={2}>
	              <Grid item xs={12} sm={6}>
	                <TextField
	                  name="nombre_autor"
	                  required
	                  fullWidth
	                  id="nombre_autor"
	                  label="Nombre Autor"
	                  onChange={(ValueForm)=>setNombre_autor(ValueForm.target.value)}
					  value={autor}
	                  autoFocus
	                />
	              </Grid>
	              <Grid item xs={12} sm={6}>
	                <TextField
	                  required
	                  fullWidth
	                  id="nombre_lugar"
	                  label="Lugar"
	                  name="nombre_lugar"
	                  onChange={(ValueForm)=>setNombre_lugar(ValueForm.target.value)}
					  value={lugar}
	                />
	              </Grid>
	              <Grid item xs={12}>
	                <TextField
	                  required
	                  fullWidth
	                  id="titulo"
	                  label="Titulo de la Noticia"
	                  name="titulo"
	                  onChange={(ValueForm)=>setTitulo_noticia(ValueForm.target.value)}
					  value={titulo_noticia}
	                />
	              </Grid>
	              <Grid item xs={12}>
	                <TextField
	                  required
	                  fullWidth
	                  name="contenido"
	                  label="Contenido"
	                  type="contenido"
	                  id="contenido"
	                  multiline
	                  onChange={(ValueForm)=>setContenido(ValueForm.target.value)}
					  value={contenido}
	                />
	              </Grid>
	              {showUploadButton && (
	              <Grid item justifyContent="flex-start">
	                <Button 
	                	component="label" 
	                	variant="contained" 
	                	name="imagen"
	                	startIcon={<CloudUploadIcon />}>
	                  Upload file
	                  <VisuallyHiddenInput type="file" />
	                </Button>
	              </Grid>
	              )}
	            </Grid>
	            
	            <Grid container justifyContent="flex-start">
	              <Grid item>
	                <Button
	                  type="submit"
	                  fullWidth
	                  variant="contained"
	                  sx={{ mt: 3, mb: 2 }}
	                >
	                  Guardar
	                </Button>
	              </Grid>
	            </Grid>
	          </Box>
	        </Box>
	      </Container>
	    </ThemeProvider>
	)
}
export default CreateNew;