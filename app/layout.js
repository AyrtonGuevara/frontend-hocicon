import Navigation from "@/components/navigation";

export const metadata={
	title: 'El Hocicón',
	description: 'Ventana de noticias',
}

export default function RootLayout({children}){
	return(
		<html lang="en">
			<body>
				<Navigation />
				{children}
			</body>
		</html>
	)
}