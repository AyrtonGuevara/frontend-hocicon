This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# El Hocicón - Backend - Desarrollo
1. Clonar el proyecto
2. ```npm install```
3. Levantar backend
4. habilitar el puerto 3000
5. Inicializar: ```npm run dev```
